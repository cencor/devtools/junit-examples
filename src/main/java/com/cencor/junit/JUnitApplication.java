/**
 * 
 */
package com.cencor.junit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author gusvmx
 *
 */
@SpringBootApplication
@EnableSwagger2
public class JUnitApplication {

    /***/
    private static final Logger LOGGER = LoggerFactory.getLogger(JUnitApplication.class);
    
    /**
     * @param args
     */
    public static void main(final String[] args) {
        SpringApplication.run(JUnitApplication.class);
        LOGGER.info("Aplicacion inicializada");
    }

}
