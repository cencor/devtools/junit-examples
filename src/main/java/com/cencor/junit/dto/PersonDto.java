package com.cencor.junit.dto;

import com.cencor.junit.entity.Person;

public class PersonDto {
    /***/
    private String name;
    /***/
    private String lastName;
    /***/
    private String fullName;

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void toContractPersonDto(final Person person) {
        this.name = person.getName();
        this.lastName = person.getLastName();
        this.fullName = this.name + " " + this.lastName;
    }
}
