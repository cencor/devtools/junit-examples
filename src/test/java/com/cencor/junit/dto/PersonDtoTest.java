package com.cencor.junit.dto;

import com.cencor.junit.entity.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

class PersonDtoTest {
    @Test
    void validateContractFromEntity() {
        //given
        Person person = new Person("Alejandro", "Hernández");
        //when
        PersonDto personDto = new PersonDto();
        personDto.toContractPersonDto(person);
        //then
        Assertions.assertAll(
                () -> assertThat(personDto.getName()).isEqualTo("Alejandro"),
                () -> assertThat(personDto.getLastName()).isEqualTo("Hernández"),
                () -> assertThat(personDto.getFullName()).isEqualTo("Alejandro Hernández")
        );
    }
}