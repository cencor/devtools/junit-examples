package com.cencor.junit.repository;

import com.cencor.junit.entity.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@DataJpaTest
class PersonRepositoryTest {
    @Autowired
    private PersonRepository underTest;

    /***/
    @Test
    final void saveTestAndGetByNameAndLastNameTest() {
        //given
        Person test = new Person("Alejandro", "Hernández");
        underTest.save(test);
        //when
        Optional<Person> byNameAndLastName = underTest.findByNameAndLastName("Alejandro", "Hernández");
        //then
        Person person = byNameAndLastName.orElse(new Person());
        Assertions.assertAll(
                () -> assertThat(byNameAndLastName).isPresent(),
                () -> assertThat(person.getName()).isEqualTo("Alejandro"),
                () -> assertThat(person.getLastName()).isEqualTo("Hernández")
        );
    }
}

